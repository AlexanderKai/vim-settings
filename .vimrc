set runtimepath+=$HOME/.vim/vimerl
autocmd Filetype erlang 
setlocal omnifunc=erlang_complete#Complete

set t_Co=256   " This is may or may not needed.

set background=dark
colorscheme PaperColor
"color solas

syntax on
map <F5> [I:let nr = input("Which one: ")<Bar>exe "normal " . nr ."[\t"<CR>

set iskeyword=@,48-57,_,192-255
set langmap=!\\"№\\;%?*ёйцукенгшщзхъфывапролджэячсмитьбюЁЙЦУКЕHГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ;!@#$%&*`qwertyuiop[]asdfghjkl\\;'zxcvbnm\\,.~QWERTYUIOP{}ASDFGHJKL:\\"ZXCVBNM<>

set laststatus=2   " всегда показывать строку статуса
set statusline=%f%m%r%h%w\ %y\ enc:%{&enc}\ ff:%{&ff}\ fenc:%{&fenc}%=(ch:%3b\ hex:%2B)\ col:%2c\ line:%2l/%L\ [%2p%%]

" перенос по словам, а не по буквам
set linebreak
set dy=lastline

" отображение выполняемой команды
set showcmd 

imap <F5> <Esc> :tabe <CR> 
map <F5> <Esc> :tabe <CR>

imap <F7> <Esc> :tabprev <CR>i
map <F7> :tabprev <CR>

imap <F8> <Esc> :tabnext <CR>i
map <F8> :tabnext <CR>

set enc=utf-8
set fileencoding=utf-8
set fileencodings=ucs-bom,utf8,prc

set tabstop=4
set shiftwidth=4
set autoindent

:set foldenable
":set foldmethod=syntax
:let erlang_folding = 1

set nocompatible              " be iMproved, required
" filetype on                  " required

map <C-n> :NERDTreeToggle<CR>


" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')
" All of your Plugins must be added before the following line

Plugin 'tucnak/vim-playfount'

Plugin 'VundleVim/Vundle.vim'

Plugin 'scrooloose/nerdtree'

Plugin 'ctrlpvim/ctrlp.vim'

Plugin 'easymotion/vim-easymotion'

Plugin 'edkolev/erlang-motions.vim'

Plugin 'vim-voom/voom'

"Plugin 'damage220/solas.vim'

Plugin 'jimenezrick/vimerl'

call vundle#end()            " required

filetype plugin indent on    " required

"Easy motion
"-------------------------------------------------------------------------
let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Jump to anywhere you want with minimal keystrokes, with just one key binding.
" `s{char}{label}`
nmap s <Plug>(easymotion-overwin-f)
" or
" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
nmap s <Plug>(easymotion-overwin-f2)

" Turn on case insensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
"-------------------------------------------------------------------------

set ttimeoutlen=50

""""""""""""""""""""""""""""
" Ranger style marks command
"
""""""""""""""""""""""""""""
function! Marks()
    marks
    echo('Mark: ')

    " getchar() - prompts user for a single character and returns the chars
    " ascii representation
    " nr2char() - converts ASCII `NUMBER TO CHAR'

    let s:mark = nr2char(getchar())
    " remove the `press any key prompt'
    redraw

    " build a string which uses the `normal' command plus the var holding the
    " mark - then eval it.
    execute "normal! '" . s:mark
endfunction

nnoremap ` :call Marks()<CR>

let g:voom_python_versions = [2,3]

"au BufRead,BufNewFile *.fountain set filetype=fountain
"au BufRead,BufNewFile *.pago    set filetype=pago 
au BufRead,BufNewFile *.fountain set filetype=fountain
